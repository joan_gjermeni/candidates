<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$mod_strings = Array(
	'Candidates' => 'Candidate',
	'Name' => 'Name',
	'SINGLE_Candidates' => 'Candidate',
	'Candidates ID' => 'Candidate ID',
	'Last Name' => 'Last Name',
	'Candidate No' => 'Candidate No',
	'Birthdate' => 'Birthdate',
	'Birthplace' => 'Birthplace',
	'Requested Position' => 'Requested Position',
	'From' => 'From',
	'Application Date' => 'Application Date',
	'Diploma Type' => 'Diploma Type',
	'Graduation' => 'Graduation',
	'Year of Graduation' => 'Year of Graduation',
	'Mobile' => 'Mobile',
	'Home Phone' => 'Home Phone',
	'Other Phone' => 'Other Phone',
	'Fax' => 'Fax',
	'Email' => 'Email',
	'Other Email' => 'Other Email',
	'Mailing Street' => 'Mailing Street',
	'Mailing PO Box' => 'Mailing PO Box',
	'Mailing City' => 'Mailing City',
	'Mailing State' => 'Mailing State',
	'Mailing Postal Code' => 'Mailing Postal Code',
	'Mailing Country' => 'Mailing Country',
	'Other Street' => 'Other Street',
	'Other PO Box' => 'Other PO Box',
	'Other City' => 'Other City',
	'Other State' => 'Other State',
	'Other Postal Code' => 'Other Postal Code',
	'Other Country' => 'Other Country',

	'LBL_CANDIDATE_INFORMATION' => 'Candidate Information',
	'LBL_PERSONALIZED_INFORMATION' => 'Personalized Information',
	'LBL_ADDRESS_INFORMATION' => 'Address Information',
	'LBL_DESCRIPTION_INFORMATION' => 'Description',

);

?>
